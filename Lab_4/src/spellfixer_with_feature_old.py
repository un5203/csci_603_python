'''
FileName: spellfixer.py

This python program reads text from standard input. It will correct text input errors
that occur because a user's finger accidentally hit a key adjacent to the intended
one instead of the intended one.

'''

__author__ = "Ujwal Bharat Nagumantri"
__author__ = "Yeshwanth Raja"

library = []

def dictionaryOfWords(words):
    '''
    This function creates a list of legal words.
    :param words:
    :return:
    '''
    processFile('words.txt',words)


def replacements(fileName,dict):
    '''
    This function checks creates a list of possible replacements for various keys.
    :param fileName: the file from which possible replacements are listed for each key
    :param dict: the file into which the replacements are
    :return: None
    '''
    array=[]
    processFile(fileName,array)
    for line in array:
        dict[line[0:1]] = line[1:].strip().split(" ")

def isWordInDictionary(word,dictionary):
    '''
    Checks if a word is legal
    :param word:
    :param dictionary:
    :return:
    '''
    for str in dictionary:
        if str.lower() == word.lower():
            return [True,str]
    return [False,word]

def checkWord(words,wordToCheck):
    '''
    This function returns a list of legal words that could be replaced
    :param words:
    :param wordToCheck:
    :return:
    '''
    replacementWords = []
    temp = isWordInDictionary(wordToCheck,words)
    if temp[0]:
        replacementWords.append(temp[1])
        return [replacementWords,True]
    else:
        for str in words:
            if len(wordToCheck)==len(str) and levDist(wordToCheck,str)==1 :
                    replacementWords.append(str)
        return [replacementWords,False]


def isClosest(incorrWord,corrWord,dict):
    '''
    This function takes two words as input and checks how close they are to each other
    :param incorrWord:
    :param corrWord:
    :param dict:
    :return:
    '''
    for i in range(len(incorrWord)):
        if incorrWord[i] != corrWord[i]:
            if isReplacement(incorrWord[i],corrWord[i],dict):
                return True
            else:
                return False

def isReplacement(a,b,dict):
    '''
    This function checks if the second parameter can be a replacement of the first parameter(typo) where both the
    parameters are alphabets.
    :param a:
    :param b:
    :param dict: list of possible replacements
    :return: True if a can be replaced by b
    '''
    '''
    check if the typo is a part of the replacement array of the correct letter
    '''
    try:
        for i in dict[a]:
            if i == b:
                return True
        return False
    except KeyError:
        return False

# def correctCase(word,isFirstWord):
#     '''
#     This function corrects the case due to holding shift key at the wrong time
#     :param word: The word whose case needs to be corrected
#     :param isFirstWord: True if the first parameter is the first word of the input string
#     :return:
#     '''
#     if isFirstWord == True:
#         temp = ""
#         for i in range(len(word)):
#             if i == 0:
#                 temp += word[i].upper()
#             else:
#                 temp += word[i].lower()
#         return temp
#     else:
#         temp = ""
#         word = word.lower()
#         temp = spellFix(word)
#         if temp == word:
#             return temp
#         else:
#             for i in range(len(word)):
#                 temp += word[i].lower()
#             return temp


def spellFix(incorrectWord):
    '''
    Fixes the spelling of the mis-typed word
    :param incorrectWord:
    :return String: correctly spelt legal word
    '''
    words = []
    dict = {}
    replacements('keyboard.txt',dict)
    processFile('words.txt',words)
    test = checkWord(words,incorrectWord)
    temp = ""
    for word in test[0]:
        if test[1]:
            temp = word
        else:
            if isClosest(incorrectWord,word,dict):
                temp = word
                break
    if temp == "":
        return incorrectWord
    else:
        return temp

def levDist(a,b):
    '''
    Calculates the Levenshtein distance between two words. In other words, it checks how closely
    the words are linked. If it returns a 1, it means that only one character of the word differs from a legal word.
    :param a: incorrect word
    :param b: correct word
    :return: levenshtein distance - we are considering only two values in this scenario. 0 or 1.
             0 represents that the words are identical and 1 represents one letter substitution is required.
    '''
    count = 0
    incorrectWord = list(a)
    correctWord = list(b)
    for i in range(len(incorrectWord)):
        if incorrectWord[i] !=correctWord[i]:
            count += 1
    return count

def processFile(fileName,list):
    """
    Process the entire text file and save it into an array.
    :param fileName (str): The file name
    "param list (list): The array into which each word is stored
    :exception: FileNotFoundError if fileName does not exist
    :return: None
    """

    # using 'with' means the file handle, f, will be automatically closed
    # when the region finishes
    with open(fileName) as f:
        # process the remainder of the lines
        for line in f:
            # strip the newline at the end of the line
            line = line.strip()
            list.append(line)


if __name__ == "__main__":
    try:
        while True:
            string = input()
            array = string.split()
            temp = ""
            for i in range(len(array)):
                if i == 0:
                    temp += spellFix(array[i].lower()) + " "
                else:
                    temp += spellFix(array[i].lower()) + " "
            temp = temp[0].upper()+temp[1:]
            print(temp)
    except EOFError:
        pass

