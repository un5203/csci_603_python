'''
FileName: spellfixer.py

This python program reads text from standard input. It will correct text input errors
that occur because a user's finger accidentally hit a key adjacent to the intended
one instead of the intended one.

'''


__author__ = "Ujwal Bharat Nagumantri"
__author__ = "Yeshwanth Raja"


# Global Constants
DICTIONARY = [] # Dictionary of legal words
TYPOS = {}      # List of possible typos

import re

def initializer():
    '''
    Generates Dictionary of words and list of possible typos
    :return:
    '''
    processFile('words.txt',DICTIONARY)
    replacements(TYPOS)


def replacements(dict):
    '''
    This function checks creates a list of possible replacements for various keys.
    :param dict: the file into which the replacements are placed in
    :return: None
    '''
    array=[]
    processFile('keyboard.txt',array)
    for line in array:
        dict[line[0:1]] = line[1:].strip().split(" ")

def isWordInDictionary(word):
    '''
    Checks if a word is legal
    :param word:
    :return [a,b]: When a is True, b is a word from the dictionary
                When a is False, b is the user typed word
    '''
    for str in DICTIONARY:
        if str.lower() == word.lower():
            return [True,str]
    return [False,word]


def checkWord(wordToCheck):
    '''
    This function returns a list of legal words that could be replaced
    :param wordToCheck:
    :return [a,b]: a is possible replacement words
                   b is True when the word is found in the dictionary
    '''
    replacementWords = []
    temp = isWordInDictionary(wordToCheck)
    if temp[0]:
        replacementWords.append(temp[1])
        return [replacementWords,True]
    else:
        for str in DICTIONARY:
            if len(wordToCheck)==len(str) and levDist(wordToCheck,str)==1 :
                    replacementWords.append(str)
        return [replacementWords,False]


def isClosest(incorrWord,corrWord):
    '''
    This function takes two words as input and checks how close they are to each other
    :param incorrWord:
    :param corrWord:
    :return: True if the first parameter can be replaced by the second parameter
            where both the parameters are full words.
    '''
    for i in range(len(incorrWord)):
        if incorrWord[i] != corrWord[i]:
            if isReplacement(incorrWord[i],corrWord[i]):
                return True
            else:
                return False

def isReplacement(a,b):
    '''
    This function checks if the second parameter can be a replacement of the first parameter(typo) where both the
    parameters are alphabets.
    :param a:
    :param b:
    :return: True if a can be replaced by b
    '''
    try:
        for i in TYPOS[a]:
            if i == b:
                return True
        return False
    except KeyError:
        return False

def spellFix(incorrectWord):
    '''
    Fixes the spelling of the mis-typed word
    :param incorrectWord:
    :return String: correctly spelt legal word
    '''
    initializer()
    test = checkWord(incorrectWord)
    temp = ""
    for word in test[0]:
        if test[1]:
            temp = word
        else:
            if isClosest(incorrectWord,word):
                temp = word
                break
    if temp == "":
        return incorrectWord
    else:
        return temp

def levDist(a,b):
    '''
    Calculates the Levenshtein distance between two words. In other words, it checks how closely
    the words are linked. If it returns a 1, it means that only one character of the word differs from a legal word.
    :param a: incorrect word
    :param b: correct word
    :return: levenshtein distance - we are considering only two values in this scenario. 0 or 1.
             0 represents that the words are identical and 1 represents one letter substitution is required.
    '''
    count = 0
    incorrectWord = list(a)
    correctWord = list(b)
    for i in range(len(incorrectWord)):
        if incorrectWord[i] != correctWord[i]:
            count += 1
    return count


def processFile(fileName,list):
    """
    Process the entire text file and save it into an array.
    :param fileName (str): The file name
    "param list (list): The array into which each word is stored
    :exception: FileNotFoundError if fileName does not exist
    :return: None
    """

    # using 'with' means the file handle, f, will be automatically closed
    # when the region finishes
    with open(fileName) as f:
        # process the remainder of the lines
        for line in f:
            # strip the newline at the end of the line
            line = line.strip()
            list.append(line)


def parseText(word,temp):
    '''
    Removes any special characters, checks the spelling and then puts them back
    :param word:
    :param temp:
    :return:
    '''

    if re.search(r'[a-z0-9]',word,re.I):
        front = re.search(r'^[\W]*',word)
        back = re.search(r'[\W]*$',word)
        if front.group() != "":
            if word.startswith(front.group()):
                    word = word[len(front.group()):]
        if back.group() != "":
            if word.endswith(back.group()):
                    word = word[:-len(back.group())]

        temp += front.group() + spellFix(word.lower())+ back.group() + " "
        return temp
    else:
        temp += word + " "
        return temp


def mainFunction():
    '''
    Corrects the spelling and typos in any sentence.
    :return: None
    '''

    try:
        while True:
            string = input()
            array = string.split()
            temp = ""
            for i in array:
                temp = parseText(i,temp)
            print(temp)
    except EOFError:
        pass


if __name__ == "__main__":
    mainFunction()

