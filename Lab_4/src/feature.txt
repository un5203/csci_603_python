The program "spellfixer_with_feature.py" corrects shift case errors in any sentence.

- Ujwal Bharat Nagumantri (un5203@rit.edu)
- Yeshwanth Raja (yr8662@rit.edu)