import turtle

def drawCircleBox1(length):
    turtle.forward(length/2)
    turtle.circle(length/2)
    turtle.forward(length/2)
    turtle.left(90)
    turtle.forward(length)
    turtle.left(90)
    turtle.forward(length)
    turtle.left(90)
    turtle.forward(length)
    turtle.left(90)

def drawCircleBox2(length):
    turtle.forward(length/2)
    turtle.circle(length/2)
    turtle.forward(length/2)
    turtle.right(45)
    drawCircleBox1(length/2)
    turtle.left(45)
    turtle.left(90)
    turtle.forward(length)
    turtle.left(90)
    turtle.forward(length)
    turtle.right(45)
    drawCircleBox1(length/2)
    turtle.left(45)
    turtle.left(90)
    turtle.forward(length)
    turtle.left(90)

def drawCircleBoxRec(length,depth):
    if depth == 0:
        return
    else:
        turtle.forward(length/2)
        turtle.circle(length/2)
        turtle.forward(length/2)
        turtle.right(45)
        drawCircleBoxRec(length/2,depth-1)
        turtle.left(45)
        turtle.left(90)
        turtle.forward(length)
        turtle.left(90)
        turtle.forward(length)
        turtle.right(45)
        drawCircleBoxRec(length/2,depth-1)
        turtle.left(45)
        turtle.left(90)
        turtle.forward(length)
        turtle.left(90)

def main():
    turtle.speed(0)
    depth = int(input('Depth: '))
    # drawCircleBox2(200)
    drawCircleBoxRec(200,depth)
    turtle.mainloop()

if __name__ == '__main__':
    main()