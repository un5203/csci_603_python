import turtle
import math

DEVIATION = 45


def drawCircleBox(n,depth):
    if depth==0:
        return
    else:
        turtle.down()
        turtle.forward(n)
        turtle.left(90)
        turtle.forward(n)
        turtle.left(90)
        turtle.forward(n)
        turtle.left(90)
        turtle.forward(n)
        turtle.left(90)
        turtle.forward(n/2)
        turtle.circle(n/2)
        turtle.backward(n/2)

        turtle.left(90)
        turtle.forward(n)
        turtle.left(45)
        turtle.up()
        drawCircleBox(n/2,depth-1)
        turtle.left(135)
        # turtle.forward(n/2)



# def drawCircleBoxRec(n,angle,depth):
#         drawCircleBox(n,angle,depth)

if __name__ == "__main__":
    turtle.speed(0)
    drawCircleBox(160,2)
    turtle.mainloop()