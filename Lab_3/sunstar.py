import turtle as t
import math as m

ANGLE = 30
RAD2 = m.sqrt(2)
PI = m.pi




def draw_side(n):
    '''
    This function draws a side of the star
    :pre: pen is up, heading east
    :post: pen is up, heading east
    :param n: accepts any positive integer
    :return: none
    '''
    t.pendown()
    t.forward(n/4)
    t.left(ANGLE)
    t.forward((n/4)* RAD2)
    t.right(ANGLE * 2)
    t.forward((n/4)* RAD2)
    t.left(ANGLE)
    t.forward(n/4)
    t.penup()

def draw_side_with_level(sideLength, level):
    '''
    This function draws a side of the star with n levels
    :pre: pen is down, heading east
    :post: pen is down, heading east
    :param sideLength: the length of the side
    :param level: depth of iterations
    :return: none
    '''
    if level == 0:
        t.forward(sideLength)
        print("drawing a side of length " + str(sideLength))
    else:
        t.forward(sideLength / 4)
        print("drawing a side of length " + str(sideLength / 4))
        t.left(ANGLE)
        print("rotating left by "+str(ANGLE)+" degrees")
        draw_side_with_level(RAD2 * sideLength / 4, level - 1)
        print("drawing a side of length " + str(RAD2 * sideLength / 4))
        t.right(ANGLE * 2)
        print("rotating right by "+str(ANGLE*2)+" degrees")
        draw_side_with_level(RAD2 * sideLength / 4, level - 1)
        print("drawing a side of length " + str(RAD2 * sideLength / 4))
        t.left(ANGLE)
        print("rotating left by "+str(ANGLE)+" degrees")
        t.forward(sideLength / 4)






def parse_number_of_sides():
    while True:
        sides = input("Enter the number of sides of sunstar: \n")

        try:
            returnValue = int(sides)
            if returnValue <= 0:
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                print("Value must be a positive integer greater than 0. You have entered "+ sides+". Try again")
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            if returnValue > 0:
                return returnValue

        except ValueError:
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("Value must be a positive integer greater than 0. You have entered "+ sides+". Try again")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")


def parse_sideLength():
    while True:
        sideLength = input("Enter the side length of sun star: \n")

        try:
            returnValue = float(sideLength)
            if returnValue <= 0:
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                print("Value must be a positive float greater than 0. You have entered "+ sideLength+". Try again")
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            if returnValue > 0:
                return returnValue

        except ValueError:
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("Value must be a positive float greater than 0. You have entered "+ sideLength+". Try again")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def parse_level():
    while True:
        level = input("Enter the number of levels of recursion: \n")

        try:
            returnValue = int(level)
            if returnValue < 0:
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                print("Value must be a positive integer greater than or equal to 0. You have entered "+ level+". Try again")
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            if returnValue >= 0:
                return returnValue

        except ValueError:
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("Value must be a positive integer greater than or equal to 0. You have entered "+ level+". Try again")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")


def parse_angle():
    while True:
        angle = input("Enter the deviation angle of the sunstar's sides in radians: \n")
        try:
            if angle.find("/") != -1:
                temp = angle.split("/")
                returnValue = float(temp[len(temp)-1])
                if returnValue < 0:
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                    print("Value must be a radian and should be between /0 (pi/0) and /2 (pi/2). You have entered "+ angle+". Try again")
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                if returnValue >= 0:
                    return 180/returnValue
            else:
                returnValue = float(angle)
                if returnValue < 0:
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                    print("Value must be a positive float greater than 0 or less than 1.57 (pi/2). You have entered "+ str(angle)+". Try again")
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                if returnValue >= 0:
                    return returnValue

        except ValueError:
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("Value must be a positive integer greater than or equal to 0. You have entered "+ str(level)+". Try again")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")


    return returnValue[1]


def draw_side_with_deviation(sideLength, level,angle,totalLength):
    '''
    This function draws a side of the star with n levels
    :pre: pen is down, heading east
    :post: pen is down, heading east
    :param sideLength: the length of the side
    :param level: depth of iterations
    :return: none
    '''
    if level == 0:
        t.forward(sideLength)
        # totalLength += sideLength
        print("drawing a side of length " + str(sideLength))
        return sideLength
    else:
        t.forward(sideLength / 4)
        totalLength += (sideLength /4)
        print("drawing a side of length " + str(sideLength / 4))
        t.left(angle)
        print("rotating left by "+str(angle)+" degrees")
        totalLength += draw_side_with_deviation(RAD2 * sideLength / 4, level - 1,angle,totalLength)
        print("drawing a side of length " + str(RAD2 * sideLength / 4))
        t.right(angle * 2)
        print("rotating right by "+str(angle*2)+" degrees")
        totalLength += draw_side_with_deviation(RAD2 * sideLength / 4, level - 1,angle,totalLength)
        print("drawing a side of length " + str(RAD2 * sideLength / 4))
        t.left(angle)
        print("rotating left by "+str(angle)+" degrees")
        t.forward(sideLength / 4)
        totalLength += (sideLength /4)
    return totalLength


#
# def draw_side_with_deviation(sideLength,level,angle,totalLength):
#     '''
#     This function draws a side of the star with n levels
#     :pre: pen is down, heading east
#     :post: pen is down, heading east
#     :param sideLength: the length of the side
#     :param level: depth of iterations
#     :return: none
#     '''
#     if level == 0:
#         t.forward(sideLength)
#         # totalLength += sideLength
#         print("drawing a side of length " + str(sideLength))
#         return sideLength
#     else:
#         t.forward(sideLength / 4)
#         totalLength += (sideLength /4)
#         print("drawing a side of length " + str(sideLength / 4))
#         t.left(angle)
#         print("rotating left by "+str(angle)+" degrees")
#         totalLength += draw_side_with_deviation(sideLength / ( 4 * m.cos(angle)), level - 1,angle,totalLength)
#         print("drawing a side of length " + str(sideLength / ( 4 * m.cos(angle))))
#         t.right(angle * 2)
#         print("rotating right by "+str(ANGLE*2)+" degrees")
#         totalLength += draw_side_with_deviation(sideLength / ( 4 * m.cos(angle)), level - 1,angle,totalLength)
#         print("drawing a side of length " + str(sideLength / ( 4 * m.cos(angle))))
#         t.left(angle)
#         print("rotating left by "+str(angle)+" degrees")
#         t.forward(sideLength / 4)
#         totalLength += (sideLength /4)
#     return totalLength
#
#

def draw_sunstar(sides,sideLength,level,angle,totalLength):
    turnAngle = 180 - (180 * (sides-2) / sides)
    # turnAngle = 360/sides
    i = sides
    temp = 0
    while(i > 0):
        temp += draw_side_with_deviation(sideLength,level,angle,totalLength)
        t.right(turnAngle)

        i -= 1
    return temp



#############################################
# main function                             #
#############################################

if __name__== "__main__":
    totalLength = 0
    t.speed(0)
    t.penup()
    t.goto(0,200)
    t.pendown()
    print("################################")
    print("#    Drawing a sunstar         #")
    print("################################")
    # sides = int(input("Enter the number of sides of sunstar: "))
    sides = parse_number_of_sides()
    sideLength = parse_sideLength()
    level = parse_level()
    if level != 0:
        angle = parse_angle()
    else:
        angle = 0
        # if(type(angle == float):
    print("sides: "+str(sides))
    print("sideLength: "+str(sideLength))
    print("level: "+str(level))
    # print("angle: "+str(angle))
    totalLength += draw_sunstar(sides,sideLength,level,angle,totalLength)
    t.penup()
    print("Total length is "+ str(totalLength))
    t.goto(-300,300)
    t.write("Total Length = %.3f" % totalLength, True,  font=("Arial", 12, "bold"),align="left")
    t.hideturtle()
    t.mainloop()






