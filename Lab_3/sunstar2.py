'''
FileName: sunstar.py


This python file draws a star using recursion (fractal design) in turtle.s
'''

__author__ = "Ujwal Bharat Nagumantri"


#importing turtle and math libraries
import turtle as t
import math as m


#GLOBAL NAMED CONSTANTS
ANGLE = 30
RAD2 = m.sqrt(2)
PI = m.pi


def parse_number_of_sides():
    '''
    This function accepts the number of sides from an input and checks it for any errors
    :return: returns the number of sides in int
    '''
    while True: #infinite loop to keep accepting input until error goes away

        sides = input("Enter the number of sides of sunstar: \n")
        #try - catch block
        try:
            returnValue = int(sides)
            if returnValue <= 0:
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                print("Value must be a positive integer greater than 0. You have entered "+ sides+". Try again")
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            if returnValue > 0:
                return returnValue

        except ValueError:
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("Value must be a positive integer greater than 0. You have entered "+ sides+". Try again")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")


def parse_sideLength():
    '''
    This function accepts the length of the sunstar side from an input and checks it for any errors
    :return: returns the length of sunstar side in float
    '''
    while True:
        sideLength = input("Enter the side length of sun star: \n")

        try:
            returnValue = float(sideLength)
            if returnValue <= 0:
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                print("Value must be a positive float greater than 0. You have entered "+ sideLength+". Try again")
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            if returnValue > 0:
                return returnValue

        except ValueError:
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("Value must be a positive float greater than 0. You have entered "+ sideLength+". Try again")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def parse_level():
    '''
    This function accepts the level of recursion from an input and checks it for any errors
    :return: returns the level as int
    '''

    while True:
        level = input("Enter the number of levels of recursion: \n")

        try:
            returnValue = int(level)
            if returnValue < 0:
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                print("Value must be a positive integer greater than or equal to 0. You have entered "+ level+". Try again")
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            else:
                return returnValue
        except ValueError:
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("Value must be a positive integer greater than or equal to 0. You have entered "+ level+". Try again")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")


def parse_angle():
    '''
    This function accepts an angle in radians from an input and checks it for any errors
    :return: returns the angle in degrees float
    '''
    while True:
        angle = input("Enter the deviation angle of the sunstar's sides between (0 to pi/2) radians: \n")


        try:
            if angle.find("/") != -1: #Checks if / is present in the string
                temp = angle.split("/")
                returnValue = m.degrees(PI / float(temp[len(temp)-1])) #takes the numeric last part of the string and converts to degrees
                if returnValue < 0 or returnValue >= 90:
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                    print("Value must be a radian and should be between /0 (pi/0) and /2 (pi/2). You have entered "+ angle+". Try again")
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                else:
                    return returnValue
            else:
                returnValue = float(angle)
                if returnValue < 0 or returnValue >= 1.57:
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                    print("Value must be a positive float greater than 0 or less than 1.57 radians (pi/2). You have entered "+ angle+". Try again")
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                else:
                    return m.degrees(returnValue)

        except ValueError:
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("Value must be a positive integer greater than 0 or less than 1.57 radians (pi/2). You have entered "+ angle+". Try again")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")



def draw_side_with_deviation(sideLength, level,angle):
    '''
    This function draws a side of the star with n levels
    :pre: pen is down, heading east
    :post: pen is down, heading east
    :param sideLength: the length of the side
    :param level: depth of iterations
    :param angle: angle of deviation
    :return: length of distance travelled by turtle for one side
    '''
    if level == 0:
        t.forward(sideLength)
        # totalLength += sideLength
        print("drawing a side of length " + str(sideLength))
        return sideLength
    else:
        t.forward(sideLength / 4)
        a = (sideLength /4)
        print("drawing a side of length " + str(sideLength / 4))
        t.left(angle)
        print("rotating left by "+str(angle)+" degrees")
        b = draw_side_with_deviation(RAD2 * sideLength / 4, level - 1,angle)
        print("drawing a side of length " + str(RAD2 * sideLength / 4))
        t.right(angle * 2)
        print("rotating right by "+str(angle*2)+" degrees")
        c = draw_side_with_deviation(RAD2 * sideLength / 4, level - 1,angle)
        print("drawing a side of length " + str(RAD2 * sideLength / 4))
        t.left(angle)
        print("rotating left by "+str(angle)+" degrees")
        t.forward(sideLength / 4)
        d = (sideLength /4)
    return a+b+c+d


def draw_sunstar(sides,sideLength,level,angle):
    '''
    This function draws a sunstar using recursion
    :param sides: Number of sides of the sunstar
    :param sideLength: Length of the side of sunstar
    :param level: depth of recursion
    :param angle: angle of deviation
    :return: returns the total length of distance travelled by the turtle while drawing the sunstar
    '''
    turnAngle = 180 - (180 * (sides-2) / sides) #formula to calculate angle of a polygon
    i = sides
    temp = 0
    while(i > 0):
        temp += draw_side_with_deviation(sideLength,level,angle)
        t.right(turnAngle)
        i -= 1
    return temp



#############################################
# main function                             #
#############################################

if __name__== "__main__":
    totalLength = 0
    t.speed(0)
    t.penup()
    t.goto(0,200)
    t.pendown()
    print("################################")
    print("#    Drawing a sunstar         #")
    print("#############################3###")
    sides = parse_number_of_sides()
    sideLength = parse_sideLength()
    level = parse_level()

    #only ask for angle if level != 0
    if level != 0:
        angle = parse_angle()
    else:
        angle = 0
    print("sides: "+str(sides))
    print("sideLength: "+str(sideLength))
    print("level: "+str(level))
    totalLength = draw_sunstar(sides,sideLength,level,angle)
    t.penup()
    print("Total length is "+ str(totalLength))
    t.goto(-300,300)
    #Print the total length on main screen
    t.write("Total Length = %.3f" % totalLength, True,  font=("Arial", 16, "bold"),align="left")
    t.hideturtle()
    t.mainloop()






