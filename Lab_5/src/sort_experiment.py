'''
Experiment to measure the performance of Selection, Insertion, Merge, Quick and Default Python Tim sort algorithms.
@author Ujwal Bharat Nagumantri
@author Yeshwanth Raja
'''

import time # Module responsible for measuring the duration of the sort
import instrumented_sort as sorter  # File from which selection, insertion, merge and quick sort algorithms are imported.

def sortUsingEverything(fileName):
    '''
    Runs a file through every sorting algorithm under discussion.
    :param fileName: The file whose contents need to be sorted.
    :return:
    '''
    print("=====================================================================================================================================")
    print("PERFORMING SORTING ON "+fileName+" TEXT FILE USING ALL ALGORITHMS")
    sortUsingSelection(fileName)
    sortUsingInsertion(fileName)
    sortUsingMerge(fileName)
    sortUsingQuick(fileName)
    sortUsingTim(fileName)
    print("SORTING ON "+fileName+" DONE !!")
    print("=====================================================================================================================================")

def sortUsingSelection(fileName):
    '''
    Runs a file through selection sorting algorithm
    :param fileName: The file whose contents need to be sorted.
    :return:
    '''
    list = []
    processFile(fileName,list)
    list,comps,timeTaken = sorter.selection_sort(list)
    print("Selection-sort algorithm was used on "+fileName+" text file and it took "+str(timeTaken*1000)+" milliseconds to sort in "+str(comps)+" computations")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def sortUsingInsertion(fileName):
    '''
    Runs a file through insertion sorting algorithm
    :param fileName: The file whose contents need to be sorted.
    :return:
    '''
    list = []
    processFile(fileName,list)
    list,comps,timeTaken = sorter.insertion_sort(list)
    print("Insertion-sort algorithm was used on "+fileName+" text file and it took "+str(timeTaken*1000)+" milliseconds to sort in "+str(comps)+" computations")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def sortUsingMerge(fileName):
    '''
    Runs a file through merge sorting algorithm
    :param fileName: The file whose contents need to be sorted.
    :return:
    '''
    list = []
    processFile(fileName,list)
    list,comps,timeTaken = sorter.merge_sort(list,comps=0,timeTaken=time.clock())
    print("Merge-sort algorithm was used on "+fileName+" text file and it took "+str(timeTaken*1000)+" milliseconds to sort in "+str(comps)+" computations")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def sortUsingQuick(fileName):
    '''
    Runs a file through Quick sorting algorithm
    :param fileName: The file whose contents need to be sorted.
    :return:
    '''
    list = []
    processFile(fileName,list)
    list,comps,timeTaken = sorter.quick_sort(list,comps=0,timeTaken=time.clock())
    print("Quick-sort algorithm was used on "+fileName+" text file and it took "+str(timeTaken*1000)+" milliseconds to sort in "+str(comps)+" computations")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def sortUsingTim(fileName):
    '''
    Runs a file through default python's sorting algorithm (Tim Sort)
    :param fileName: The file whose contents need to be sorted.
    :return:
    '''
    list = []
    processFile(fileName,list)
    startTime = time.clock()
    sorted(list)
    timeTaken = time.clock()-startTime
    print("The default built-in sorting algorithm of python was used on "+fileName+" text file and it took "+str(timeTaken*1000)+" milliseconds to sort.")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")




def processFile(fileName,list):
    """
    Process the entire text file and save it into an array.
    :param fileName (str): The file name
    "param list (list): The array into which each word is stored
    :exception: FileNotFoundError if fileName does not exist
    :return: None
    """

    # using 'with' means the file handle, f, will be automatically closed
    # when the region finishes
    with open(fileName) as f:
        # process the remainder of the lines
        for line in f:
            # strip the newline at the end of the line
            line = line.strip()
            list.append(line)


if __name__ == "__main__":
    fileNames = ["words1.txt","words1-sorted.txt","words1-reversed.txt","words2.txt","words2-sorted.txt",
                 "words2-reversed.txt","words3.txt","words3-sorted.txt","words3-reversed.txt"]
    for file in fileNames:
        sortUsingEverything(file)
