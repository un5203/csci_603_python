"""
Sorting algorithms

Original Authors: Zack Butler, James Heliotis 2015-16

Student revisions by YOUR NAMES HERE

"""

import random

def selection_sort( data ):
    """
    Sort the provided data using the selection-sort algorithm.
    :param data: a mutable sequence of comparable values
    :post: The data collection provided is modified so that the elements
           are in sorted order.
    :return: The modified data parameter object (for compatibility)
    """
    size = len( data )
    for next_pos in range( 0, size-1 ):
        # Invariant: The data up to but not including index next_pos is sorted
        #            _in_its_final_form_.
        assert is_sorted( data[ :next_pos ] )
        # Note that when next_pos = size-1 we have left the loop. (Good!)
        #
        # Now, select the value that should go at position next_pos.
        min_pos = next_pos
        min_so_far = data[ min_pos ]
        for i in range( next_pos+1, size ):
            next_value = data[ i ]
            if next_value < min_so_far:
                min_so_far = next_value
                min_pos = i

        # Invariant: the smallest value in the unsorted side
        #  is in position min_pos
        assert data[ min_pos ] == min( data[ next_pos: ] )

        # Swap that minimum value with the one where the minimum value belongs
        temp = data[ next_pos ]
        data[ next_pos ] = data[ min_pos ]
        data[ min_pos ] = temp
    return data

def insertion_sort( original_data ):
    """
    Perform an insertion sort on the provided data. Warning: The data is
    modified, but it is also returned to be compatible with the
    non-destructive sort functions in this module.
    :param original_data: a MutableSequence of comparable elements
    :return: the original_data sequence
    """

    #
    # NOTE: Some data sets will take an unnecessarily long time to be sorted
    # due to a tiny inefficiency in this algorithm.
    #

    data = original_data[:]
    # k is the number of elements sorted so far.
    # Thus, k is the index where we get the next one to insert.
    for k in range( 1, len( data ) ):
        # Invariant: Locations 0..k-1 are sorted.
        # k is the location of the element to be inserted
        # into the sorted elements.
        new = data[ k ]
        for index in range( k - 1, -1, -1 ):
            if data[ index ] < new:
                # We have found the spot.
                break
            else:
                # Swap the new value down one spot and continue.
                data[ index + 1 ] = data[ index ]
                data[ index ] = new
        # Loop exits when new value has been shifted down to
        # the right spot in the sorted data.
    return data

def merge_sort( data ):
    """
    Sort the provided data using the selection-sort algorithm.
    :param data: a sequence of comparable values
    :post: The original data sequence has not changed.
    :return: A copy of data, but with the elements in sorted order
    """
    result = []
    # Split the array in half.
    mid_pos = len( data ) // 2
    part1, part2 = data[ :mid_pos ], data[ mid_pos: ]
    if len( part1 ) > 1:
        part1 = merge_sort( part1 )
    if len( part2 ) > 1:
        part2 = merge_sort( part2 )
    # Invariant: part1 and part2 are now sorted.
    assert is_sorted( part1 ) and is_sorted( part2 )

    # Merge the two sorted parts together.
    len1 = len( part1 )
    len2 = len( part2 )
    i1 = 0
    i2 = 0
    while i1 < len1 and i2 < len2:
        # Keep merging as long as NEITHER part has been non-emptied.
        part1i1 = part1[ i1 ]
        part2i2 = part2[ i2 ]
        if part1i1 < part2i2:
            result.append( part1i1 )
            i1 += 1
        else:
            result.append( part2i2 )
            i2 += 1
    # Invariant: at least one of the parts has been emptied.
    assert i1 == len1 or i2 == len2
    # Now add the rest of the non-empty part to the end.
    if i1 < len1:
        result.extend( part1[ i1: ] )
    if i2 < len2:
        result.extend( part2[ i2: ] )
    return result

def quick_sort( data ):
    """
    Perform a 'quick' sort on the provided data. The original data is
    unchanged.
    :param original_data: a Sequence of comparable elements
    :return: a list containing the elements of data, but in sorted order
    """
    if len( data ) <= 1:
        return data
    pivot_index = 0 # Choose first element as the one whose
    pivot = data[ pivot_index ]  # value will divide the array.
    smaller = []
    equal = [ pivot ]
    greater = []
    for value in data[ 1: ]:
        if value < pivot:
            smaller.append( value )
        elif value == pivot:
            equal.append( value )
        else:
            greater.append( value )
    return quick_sort( smaller ) + equal + quick_sort( greater )

def is_sorted( data ):
    """
    Is the data sorted?
    :param data" a Sequence of comparable elements
    :return: True iff the elements are in order (according to __le__, ie, "<=".)
    """
    return True # TODO not yet implemented!!!

def test():
    """
    Check to make sure all of the above sorts, plus the built-in sorted
    function, are working.
    (Note: the sorted function uses 'Tim Sort'.)
    :return:
    """
    nums = []
    size = 10000
    for _ in range( size ):
        nums.append( random.randint( 0, size ) )
    for sorter in \
            selection_sort, insertion_sort, merge_sort, quick_sort, sorted:
        data = nums[:]
        print( "Running " + sorter.__name__ + ":" )
        sortednums = sorter( data )
        print( "Success!" if is_sorted( sortednums ) else "BROKEN SORT" )
        print()

if __name__ == "__main__":
    test()

