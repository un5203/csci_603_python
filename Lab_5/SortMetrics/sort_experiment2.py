import sys
sys.setrecursionlimit(10000)
import time
import instrumented_sort as sorter


def sortUsingEverything(fileName):
    print("=====================================================================================================================================")
    print("PERFORMING SORTING ON "+fileName+" TEXT FILE USING ALL ALGORITHMS")
    sortUsingTim(fileName)
    sortUsingQuick(fileName)
    sortUsingMerge(fileName)
    # sortUsingInsertion(fileName)
    # sortUsingSelection(fileName)
    print("SORTING ON "+fileName+" DONE !!")
    print("=====================================================================================================================================")


def sortUsingSelection(fileName):
    list = []
    processFile(fileName,list)
    list,comps,timeTaken = sorter.insertion_sort(list)
    print("Insertion-sort algorithm was used on "+fileName+" text file and it took "+str(timeTaken)+" milliseconds to sort in "+str(comps)+" computations")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def sortUsingInsertion(fileName):
    list = []
    processFile(fileName,list)
    list,comps,timeTaken = sorter.insertion_sort(list)
    print("Insertion-sort algorithm was used on "+fileName+" text file and it took "+str(timeTaken)+" milliseconds to sort in "+str(comps)+" computations")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def sortUsingMerge(fileName):
    list = []
    processFile(fileName,list)
    list,comps,timeTaken = sorter.merge_sort(list,comps=0,timeTaken=time.clock())
    print("Merge-sort algorithm was used on "+fileName+" text file and it took "+str(timeTaken)+" milliseconds to sort in "+str(comps)+" computations")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def sortUsingQuick(fileName):
    list = []
    processFile(fileName,list)
    list,comps,timeTaken = sorter.quick_sort(list,comps=0,timeTaken=time.clock())
    print("Quick-sort algorithm was used on "+fileName+" text file and it took "+str(timeTaken)+" milliseconds to sort in "+str(comps)+" computations")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

def sortUsingTim(fileName):
    list = []
    processFile(fileName,list)
    startTime = time.clock()
    sorted(list)
    timeTaken = time.clock()-startTime
    print("The default built-in sorting algorithm of python was used on "+fileName+" text file and it took "+str(timeTaken)+" milliseconds to sort.")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")




def processFile(fileName,list):
    """
    Process the entire text file and save it into an array.
    :param fileName (str): The file name
    "param list (list): The array into which each word is stored
    :exception: FileNotFoundError if fileName does not exist
    :return: None
    """

    # using 'with' means the file handle, f, will be automatically closed
    # when the region finishes
    with open(fileName) as f:
        # process the remainder of the lines
        for line in f:
            # strip the newline at the end of the line
            line = line.strip()
            list.append(line)


if __name__ == "__main__":
    fileNames = ["2cities.txt","2cities-reversed.txt","2cities-sorted.txt","both.txt","both-reversed.txt",
                 "both-sorted.txt","odyssey.txt","odyssey-reversed.txt","odyssey-sorted.txt"]
    # for file in fileNames:
    #     sortUsingQuick(file)
    sortUsingQuick(fileNames[2])