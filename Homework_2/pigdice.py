"""
file: pigdice.py
language: python3
description: Contains the game logic of PigDice game
"""

__author__ = 'Ujwal Bharat Nagumantri, Yeshwanth Raja'
__author_email__ = 'un5203@rit.edu, yr8662@rit.edu'

import turtle
import random

from score import Keeper

# window dimensions
WINDOW_WIDTH = 800
WINDOW_HEIGHT = 600
SIDE = 150 #size of the die
scorecard = Keeper()


def init():
    """
    Initialize for drawing.(-800, -600) is in the lower left and
    (800, 600) is in the upper right.
    :pre: pen up heading (east)
    :post: pen up heading (east)
    :return: None
    """

    turtle.setworldcoordinates(-WINDOW_WIDTH, -WINDOW_WIDTH,
                               WINDOW_WIDTH, WINDOW_HEIGHT)
    turtle.penup()
    turtle.setheading(0)
    turtle.title('TheGame')
    print("Starting with Player 0")
    turtle.speed(0)


def drawbutton():
    """
    This function draws a button
    :pre: turtle is penup and facing east
    :post: turtle is penup and facing east
    :return:
    """
    turtle.goto(0, -WINDOW_HEIGHT / 2)
    turtle.pendown()
    turtle.circle(50)
    turtle.penup()
    turtle.goto(0, -WINDOW_HEIGHT / 2 + 25)
    turtle.pendown()
    turtle.write("HOLD", False, align="center", font=("Arial", 10, "bold"))
    turtle.penup()
    turtle.goto(-75, 0)
    turtle.setheading(0)


def draw_outline():
    """
    Draws the outline of the die
    :pre: pen up facing east
    :post: pen up facing east
    :return: None
    """
    turtle.pendown()
    turtle.forward(SIDE)
    turtle.left(90)
    turtle.forward(SIDE)
    turtle.left(90)
    turtle.forward(SIDE)
    turtle.left(90)
    turtle.forward(SIDE)
    turtle.left(90)
    turtle.penup()


def button_click(x, y):
    """
    Specifying click region for button and die
    :param x: x coordinate
    :param y: y coordinate
    :return: None
    """

    # Specifying boundary for Die
    if ((x > -77) and (x < 77)) and ((y > 2) and (y < 150)):
        draw_die()
    # Specifying boundary for Hold Button
    elif ((x > -60) and (x < 60)) and ((y < -200) and (y > -300)):
        print("Player "+ str(scorecard.player)+" has pressed hold")
        hold()


def hold():
    """
    This function is run when the player presses hold.
    It displays the current player's score and switches to a new player
    :return:
    """
    scorecard.score[scorecard.player] += scorecard.points;
    print("Player " + str(scorecard.player) + "'s score is "+ str(scorecard.score[scorecard.player]))
    scorecard.switch_player()
    turtle.clear()
    drawbutton()
    draw_outline()
    print("Switching player !")
    print("Starting with Player " + str(scorecard.player))


def draw_center_dot():
    """
    To draw center dot in the die
    :pre: turtle pen up facing east
    :post: turtle pen up facing east
    :return: None
    """

    turtle.forward(SIDE / 2)
    turtle.left(90)
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.backward(SIDE / 2)
    turtle.right(90)
    turtle.backward(SIDE / 2)


def draw_two_dots():
    """
    Drawing two dots
    :pre: turtle pen up facing east
    :post: turtle pen up facing east
    :return:None
    """
    turtle.forward(SIDE / 4)
    turtle.left(90)
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.right(90)
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.backward(SIDE / 2)
    turtle.backward(SIDE / 4)
    turtle.right(90)
    turtle.forward(SIDE / 2)
    turtle.setheading(0)


def draw_three_dots():
    """
    Draw 3 dots of the die
    :pre: turtle pen up facing east
    :post: turtle pen up facing east
    :return: None
    """
    draw_two_dots()
    draw_center_dot()


def draw_four_dots():
    """
    Draw 4 dots of the die
    :pre:turtle pen up facing east
    :post: turtle pen up facing east
    :return: None
    """
    turtle.forward(SIDE / 4)
    turtle.left(90)
    turtle.forward(SIDE / 4)
    turtle.dot()
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.right(90)
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.right(90)
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.forward(SIDE / 4)
    turtle.right(90)
    turtle.forward(SIDE / 2)
    turtle.forward(SIDE / 4)
    turtle.setheading(0)


def draw_five_dots():
    """
    Draw 5 dots of the die
    :pre:turtle pen up facing east
    :post: turtle pen up facing east
    :return: None
    """
    draw_center_dot()
    draw_four_dots()


def draw_six_dots():
    """
    Draw 6 dots of the die
    :pre:turtle pen up facing east
    :post: turtle pen up facing east
    :return: None
    """
    draw_four_dots()
    turtle.forward(SIDE / 2)
    turtle.left(90)
    turtle.forward(SIDE / 4)
    turtle.dot()
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.backward(SIDE / 2)
    turtle.backward(SIDE / 4)
    turtle.right(90)
    turtle.backward(SIDE / 2)


def draw_die():
    """
    Creating the conditions for drawing the die face based on the random number generated
    :return:None
    """
    turtle.clear()
    random_number = random.randrange(1, 7)
    if (random_number == 1):
        drawbutton()
        draw_outline()
        draw_center_dot()
        hold()
    elif (random_number == 2):
        drawbutton()
        draw_outline()
        draw_two_dots()
        scorecard.add_points(2)
    elif (random_number == 3):
        drawbutton()
        draw_outline()
        draw_three_dots()
        scorecard.add_points(3)
    elif (random_number == 4):
        drawbutton()
        draw_outline()
        draw_four_dots()
        scorecard.add_points(4)
    elif (random_number == 5):
        drawbutton()
        draw_outline()
        draw_five_dots()
        scorecard.add_points(5)
    elif (random_number == 6):
        drawbutton()
        draw_outline()
        draw_six_dots()
        scorecard.add_points(6)
    #print("Player" + str(scorecard.player) + "'s score in this turn =" + " " + str(scorecard.points))
    scorecard.score[scorecard.player] += scorecard.points;
    scorecard.points = 0;
    print("Player " + str(scorecard.player) + " is playing")
    print("Player" + str(scorecard.player) + "'s total score =" + " " + str(scorecard.score[scorecard.player]))
    print("Player 0's score : " + str(scorecard.score[0]) + " and Player 1's score : " + str(scorecard.score[1]))
    print("===============================================================================")
    if (scorecard.score[scorecard.player] >= 50):
        print("Player" + str(scorecard.player) + " " + "Wins" + " " + "Congrats!")
        turtle.clear()


def main():
    """
    The main function.
    :pre: pen up heading (east)
    :post:pen up heading (east)
    :return: Noneg
    """
    # To Initialise the turtle
    init()
    drawbutton()
    draw_outline()
    turtle.onscreenclick(button_click)
    turtle.mainloop()


if __name__ == '__main__':
    main()