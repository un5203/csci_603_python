import turtle
import random

from score import Keeper

# window dimensions
WINDOW_WIDTH = 800
WINDOW_HEIGHT = 600
SIDE = 150
scorecard = Keeper()


def init():
    """
    Initialize for drawing.(-800, -600) is in the lower left and
    (800, 600) is in the upper right.
    :pre: pen up heading (east)
    :post: pen up heading (east)
    :return: None
    """

    turtle.setworldcoordinates(-WINDOW_WIDTH, -WINDOW_WIDTH,
                               WINDOW_WIDTH, WINDOW_HEIGHT)
    turtle.penup()
    turtle.setheading(0)
    turtle.title('TheGame')
    print("Starting with Player 0")
    turtle.speed(4)


def drawbutton():
    turtle.goto(0, -WINDOW_HEIGHT / 2)
    turtle.pendown()
    turtle.circle(50)
    turtle.penup()
    turtle.goto(0, -WINDOW_HEIGHT / 2 + 25)
    turtle.pendown()
    turtle.write("HOLD", False, align="center", font=("Arial", 10, "bold"))
    turtle.penup()
    turtle.goto(-75, 0)
    turtle.setheading(0)


def draw_outline():
    """
    Creating the outline of the die
    :pre: pen up facing east
    :post: pen up facing east
    :return: None
    """
    turtle.pendown()
    turtle.forward(SIDE)
    turtle.left(90)
    turtle.forward(SIDE)
    turtle.left(90)
    turtle.forward(SIDE)
    turtle.left(90)
    turtle.forward(SIDE)
    turtle.left(90)
    turtle.penup()


def button_click(x, y):
    """
    Specifying click region for button and die
    :param x: x coordinate
    :param y: y coordinate
    :return: None
    """

    # Specifying boundary for Die
    if ((x > -77) and (x < 77)) and ((y > 2) and (y < 150)):
        draw_die()
    # Specifying boundary for Hold Button
    elif ((x > -47) and (x < 49)) and ((y > -198) and (y < -300)):
        print("hold")


def draw_center_dot():
    """
    To draw center dot in the die
    :pre: turtle pen up facing east
    :post: turtle pen up facing east
    :return: None
    """

    turtle.forward(SIDE / 2)
    turtle.left(90)
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.backward(SIDE / 2)
    turtle.right(90)
    turtle.backward(SIDE / 2)


def draw_two_dots():
    """
    Drawing two dots
    :pre: turtle pen up facing east
    :post: turtle pen up facing east
    :return:None
    """
    turtle.forward(SIDE / 4)
    turtle.left(90)
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.right(90)
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.backward(SIDE / 2)
    turtle.backward(SIDE / 4)
    turtle.right(90)
    turtle.forward(SIDE / 2)
    turtle.setheading(0)


def draw_three_dots():
    """
    Draw 3 dots of the die
    :pre: turtle pen up facing east
    :post: turtle pen up facing east
    :return: None
    """
    draw_two_dots()
    draw_center_dot()


def draw_four_dots():
    """
    Draw 4 dots of the die
    :pre:turtle pen up facing east
    :post: turtle pen up facing east
    :return: None
    """
    turtle.forward(SIDE / 4)
    turtle.left(90)
    turtle.forward(SIDE / 4)
    turtle.dot()
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.right(90)
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.right(90)
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.forward(SIDE / 4)
    turtle.right(90)
    turtle.forward(SIDE / 2)
    turtle.forward(SIDE / 4)
    turtle.setheading(0)


def draw_five_dots():
    """
    Draw 5 dots of the die
    :pre:turtle pen up facing east
    :post: turtle pen up facing east
    :return: None
    """
    draw_center_dot()
    draw_four_dots()


def draw_six_dots():
    """
    Draw 6 dots of the die
    :pre:turtle pen up facing east
    :post: turtle pen up facing east
    :return: None
    """
    draw_four_dots()
    turtle.forward(SIDE / 2)
    turtle.left(90)
    turtle.forward(SIDE / 4)
    turtle.dot()
    turtle.forward(SIDE / 2)
    turtle.dot()
    turtle.backward(SIDE / 2)
    turtle.backward(SIDE / 4)
    turtle.right(90)
    turtle.backward(SIDE / 2)


def draw_die():
    """
    Creating the conditions for drawing the die face based on the random number generated
    :return:None
    """
    turtle.clear()
    random_number = random.randrange(0, 7)
    if (random_number == 1):
        drawbutton()
        draw_outline()
        draw_center_dot()
        print("Sorry. Switching Player")
        scorecard.points = 0
        scorecard.switch_player()
    elif (random_number == 2):
        drawbutton()
        draw_outline()
        draw_two_dots()
        scorecard.add_points(2)
    elif (random_number == 3):
        drawbutton()
        draw_outline()
        draw_three_dots()
        scorecard.add_points(3)
    elif (random_number == 4):
        drawbutton()
        draw_outline()
        draw_four_dots()
        scorecard.add_points(4)
    elif (random_number == 5):
        drawbutton()
        draw_outline()
        draw_five_dots()
        scorecard.add_points(5)
    elif (random_number == 6):
        drawbutton()
        draw_outline()
        draw_six_dots()
        scorecard.add_points(6)
    print("Player" + str(scorecard.player) + "'s score =" + " " + str(scorecard.points))
    if (scorecard.points >= 50):
        print("Player" + str(scorecard.player) + " " + "Wins" + " " + "Congrats!")
        turtle.clear()
        init()
        drawbutton()
        draw_outline()


def main():
    """
    The main function.
    :pre: pen up heading (east)
    :post:pen up heading (east)
    :return: None
    """
    # To Initialise the turtle
    init()
    drawbutton()
    draw_outline()
    turtle.onscreenclick(button_click)
    turtle.mainloop()


if __name__ == '__main__':
    main()
