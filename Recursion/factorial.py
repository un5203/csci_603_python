# Demonstrations of how to implement the factorial function
#
# This code shows the similarities and differences between recursion
# and iteration.
#
# author: James Heliotis
# June 2015

#############################################################################

def fact1( n ):
    """
    Classic recursive implementation of factorial

    :param n: a non-negative integer
    :returns: n!
    """

    if n == 0:
        return 1
    else:
        return n * fact1( n - 1 )

#############################################################################

def fact2( n ):
    """
    'Start' function for recursive implementation that uses an accumulator
    This function provides the initial value of the accumulator so the caller
    does not have to know about it.

    :param n: a non-negative integer
    :returns: n!
    """

    return fact2_rec( n, 1 )

def fact2_rec( n, acc ):
    """
    Recursive implementation of factorial that uses an accumulator
    (Many recursive functions end of being more efficient when written
    this way. In this case it makes no difference.)

    :param n: a non-negative integer
    :param acc: an integer factor
    :returns: n! * acc
    """
    
    if n == 0:
        return acc
    else:
        return fact2_rec( n - 1, n * acc )

#############################################################################

def fact3( n ):
    """
    Iterative implementation of factorial that mimics the recursive
    accumulator approach
    It should be clear how fact2, which is tail recursive, is easily
    transformed into a loop.

    param n: a non-negative integer
    returns: n!
    """

    acc = 1
    while n > 0:
        acc = n * acc
        n = n - 1
    return acc

#############################################################################


def test():
    factorials = ( fact1, fact2, fact3 )

    print( "%3s" % "n", end="" )
    for func in factorials:
        print( "%8s" % func.__name__, end="" )
    print()
    print()
    
    for n in range( 11 ):
        print( "%3d" % n, end="" )
        for func in factorials:
            print( "%8d" % func( n ), end="" )
        print()
    print()

if __name__ == "__main__":
    test()
