""" 
file: hanoi.py
language: python3
description: Solve the Towers of Hanoi puzzle.
"""
__author__ = 'James Heliotis'


def hanoi( count, source, dest ):
    """
    Move discs from source tower to destination tower, assuming
    there are three towers available, numbered 1, 2, 3.
    :param count: how many discs to move
    :param source: the number of the starting tower
    :param dest: the number of the destination tower
    :return: None
    """
    assert source != dest
    assert 1 <= source <= 3
    assert 1 <= dest <= 3
    assert count > 0

    if count == 1: # base case
        print( "Move top disc from", source, "to", dest )
    else:
        spare = 6 - ( source + dest )
        hanoi( count-1, source, spare )
        hanoi( 1, source, dest )
        hanoi( count-1, spare, dest )


if __name__ == '__main__':
    hanoi( 1, 2, 3 )
    print()
    hanoi( 2, 1, 3 )
    print()
    hanoi( 4, 3, 2 )
