# An example of tail recursion

# Conversion of these functions to use a loop is trivial.
#
def xs( size ):
    if size == 0:
        print()
    else:
        print( size * "X" )
        xs( size - 1 ) # Recursive call is the very last thing executed.

if __name__ == "__main__":
    xs( 8 )

