# Compute the greatest common divisor of two natural numbers.

def gcd( a, b ):
    """ Compute the greatest divisor of two integers.
        :param a: the first integer
        :param b: the second integer
        :return: the largest integer that divides into both a and b
        :pre: a >= 0 and b >= 0
    """
    if b == 0:
        result = a
    else:
        result = gcd( b, a % b )
    return result

TEST_MAX = 12

def test():
    print( "b  a: ", end="" )
    for a in range( TEST_MAX+1 ):
        print( "%3d" % a, end="" )
    print()
    for b in range( TEST_MAX+1 ):
        print( "%3s | " % b, end="" )
        for a in range( TEST_MAX+1 ):
            print( "%3d" % gcd( a, b ), end="" )
        print()
    print()

if __name__ == "__main__":
    test()
